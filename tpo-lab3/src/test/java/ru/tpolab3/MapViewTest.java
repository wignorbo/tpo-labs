package ru.tpolab3;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

class MapViewTest {
    List<RemoteWebDriver> drivers;

    private static final String WORK_URL = "https://maps.yandex.ru";

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeEach
    public void setUp() throws MalformedURLException {
        drivers = new ArrayList<>();
        drivers.add(DriverHandler.getChromeDriver("map_view_test"));
        drivers.add(DriverHandler.getFirefoxDriver("map_view_test"));
    }

    @AfterEach
    public void tearDown() {
        drivers.forEach(RemoteWebDriver::quit);
    }

    @Test
    void testMapView() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(drivers.size());
        drivers.forEach(x -> new Thread(() -> {
            try {
                testMapViewWithDriver(x);
            } catch (InterruptedException e) {
                Assertions.fail();
            } finally {
                latch.countDown();
            }
        }).start());
        latch.await();
        Assertions.assertTrue(true);
    }

    record Coordinates(double x, double y, double z) {
    }
    private void testMapViewWithDriver(final WebDriver driver) throws InterruptedException {
        driver.get(WORK_URL);
        final MapView view = new MapView(driver);
        Thread.sleep(1000);

        testZoom(view);
        testMapTypes(view);
    }

    private void testZoom(final MapView view) throws InterruptedException {
        view.zoomIn();
        Thread.sleep(1000);
        final String urlBefore = view.getDriver().getCurrentUrl();
        Thread.sleep(1000);
        view.zoomOut();
        Thread.sleep(1000);
        final String urlAfter = view.getDriver().getCurrentUrl();

        final Coordinates before = extractCoordinates(urlBefore);
        final Coordinates after = extractCoordinates(urlAfter);

        Assertions.assertTrue(after.z < before.z);

        Thread.sleep(1000);
    }

    private void testMapTypes(final MapView view) throws InterruptedException {
        view.moveToOptions();
        view.clickSatellite();

        Assertions.assertTrue(view.getDriver().getCurrentUrl().contains("sputnik"));

        view.clickHybrid();

        Assertions.assertTrue(view.getDriver().getCurrentUrl().contains("hybrid"));
    }

    private Coordinates extractCoordinates(final String url) {
        final String z = url.split("&")[1].substring(2);
        final String x = url.split("ll=")[1].split("%2C")[0];
        final String y = url.split("%2C")[1].split("&z=")[0];

        return new Coordinates(Double.parseDouble(x),
                Double.parseDouble(y),
                Double.parseDouble(z));
    }
}
