package ru.tpolab3;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

// page_url = https://maps.yandex.ru/
public class SearchBar {

    protected final WebDriver driver;
    final private By searchBarInput = By.xpath("//div[@class=\"search-form-view__input\"]//*[contains(@class, \"input__control\")]");
    final private By resultTable = By.xpath("//ul[@class=\"search-list-view__list\"]/li[@class=\"search-snippet-view\"]");

    final private By searchTitle = By.xpath("//div[@class=\"search-business-snippet-view__title\"]");

    final private By cardTitle = By.xpath("//div[@class=\"business-card-view__main-wrapper\"]//a[@class=\"card-title-view__title-link\"]");

    final private By cardType = By.xpath("//div[@class=\"business-card-view__main-wrapper\"]//a[@class=\"business-card-title-view__category _outline\"]");

    final private By cardRating = By.xpath("//div[@class=\"business-header-rating-view__user-rating\"]//span[@class=\"business-rating-badge-view__rating-text _size_m\"]");

    final private By cardAddress = By.xpath("//div[@class=\"business-contacts-view__address\"]//div[@class=\"business-contacts-view__address-link\"]");

    final private By cardReviewButton = By.xpath("//div[@class=\"carousel__scrollable _smooth-scroll\"]//div[@class=\"tabs-select-view__title _name_reviews\"]");

    final private By cardReviewEntry = By.xpath("//div[@class=\"business-reviews-card-view__reviews-container\"]//div[@class=\"business-reviews-card-view__review\"]");

    final private By cardPhotoSlideshow = By.xpath("//div[@class=\"carousel__scrollable _snap _smooth-scroll\"]");

    final private By cardGalleryEntries = By.xpath("//div[@class=\"media-gallery__frame-wrapper\"]");

    final private By cardRouteButton = By.xpath("//div[@class=\"card-feature-view__wrapper\"]//div[@class=\"business-contacts-view__link _route _noprint _inline\"]");

    final private By cardRouteView = By.xpath("//div[@class=\"route-form-view__points\"]");

    final private By cardRouteTypeBicycle = By.xpath("//div[@class=\"route-travel-modes-view\"]//div[@aria-label=\"На велосипеде\"]");
    final private By cardRouteTypePedestrian = By.xpath("//div[@class=\"route-travel-modes-view\"]//div[@aria-label=\"Пешком\"]");


    public SearchBar(final WebDriver driver) {
        this.driver = driver;
    }

    public void searchQuery(final String query) {
        final WebElement search = driver.findElement(searchBarInput);
        search.click();
        search.sendKeys(query);
        search.sendKeys(Keys.RETURN);
    }

    public List<WebElement> getSearchResults() {
        return driver.findElements(resultTable);
    }

    public void clickOnSearchResultTitle(final int index) {
        final List<WebElement> elems = getSearchResults();
        elems.get(index).findElement(searchTitle).click();
    }

    public void clickOnRouteButton() {
        driver.findElement(cardRouteButton).click();
    }

    public void clickRouteTypePedestrian() {
        driver.findElement(cardRouteTypePedestrian).click();
    }

    public void clickRouteTypeBicycle() {
        driver.findElement(cardRouteTypeBicycle).click();
    }

    public boolean routeFormIsVisible() {
        return driver.findElement(cardRouteView).isDisplayed();
    }

    public boolean isRouteTypeBicycle() {
        return "true".equals(driver.findElement(cardRouteTypeBicycle).getAttribute("aria-pressed"));
    }

    public boolean isRouteTypePedestrian() {
        return "true".equals(driver.findElement(cardRouteTypePedestrian).getAttribute("aria-pressed"));
    }

    public String getCardTitle() {
        return driver.findElement(cardTitle).getText();
    }

    public String getCardType() {
        return driver.findElement(cardType).getText();
    }

    public String getCardRating() {
        return driver.findElement(cardRating).getText();
    }

    public String getCardAddress() {
        return driver.findElement(cardAddress).getText();
    }

    public List<WebElement> getCardReviews() {
        driver.findElement(cardReviewButton).click();
        final List<WebElement> elements = driver.findElements(cardReviewEntry);
        driver.findElement(By.xpath("//div[@class=\"tabs-select-view__title _name_overview\"]")).click();
        return elements;
    }

    public List<WebElement> getCardPhotoGallery() {
        driver.findElement(cardPhotoSlideshow).click();
        final List<WebElement> photos = driver.findElements(cardGalleryEntries);
        driver.findElement(By.xpath("//div[@class=\"photos-player-view__button _type_close\"]")).click(); // close to make idempotent
        return photos;
    }
}