package ru.tpo.func.trig;

import ru.tpo.func.CSVWritable;

public class Secant extends BaseTrigFunction implements CSVWritable {
    private final Cosine cos;

    public Secant() {
        super();
        this.cos = new Cosine();
    }

    public Secant(final Cosine cos) {
        super();
        this.cos = cos;
    }

    @Override
    public double evaluate(final double x, final double eps) {
        if (Math.abs(Math.abs(x) % Math.PI - Math.PI / 2) < eps) {
            return Double.NaN;
        }
        return 1 / cos.evaluate(x, eps);
    }
}
