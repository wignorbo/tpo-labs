package ru.tpo.func.unit.log;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;
import ru.tpo.func.log.LogBaseN;

import static org.junit.jupiter.api.Assertions.*;

class LogBaseNUnitTest {

    final LogBaseN logN = new LogBaseN();

    @ParameterizedTest
    @CsvFileSource(resources = "/log5.csv", delimiter = ',', numLinesToSkip = 1)
    void testLog5(final double arg, final double res) {
        assertEquals(res, logN.evaluate(5, arg), logN.eps * 10);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/log10.csv", delimiter = ',', numLinesToSkip = 1)
    void testLog10(final double arg, final double res) {
        assertEquals(res, logN.evaluate(10, arg), logN.eps * 10);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-5, -4, -3, -2, -1, 1})
    void testIncorrectBase(final double base) {
        assertEquals(Double.NaN, logN.evaluate(base, 2));
    }

    @ParameterizedTest
    @ValueSource(doubles = {-5, -4, -3, -2, -1, 0})
    void testIncorrectArgument(final double arg) {
        assertEquals(Double.NaN, logN.evaluate(2, arg));
    }
}