package lab1.domain_model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DolphinTest {

    static final int GOOD_AGE = 10;
    static final int GOOD_EXCITEMENT = 100;
    static final String GOOD_NAME = "Name";


    @ParameterizedTest
    @MethodSource("generateBadNames")
    void givenNonAlphabeticNameWhenConstructingDolphinThenIaeThrown(final String name) {
        assertThrows(IllegalArgumentException.class, () -> new Dolphin(name, GOOD_AGE, GOOD_EXCITEMENT));
    }

    @ParameterizedTest
    @MethodSource("generateBadAges")
    void givenBadAgeWhenConstructingDolphinThenIaeThrown(final int age) {
        assertThrows(IllegalArgumentException.class, () -> new Dolphin(GOOD_NAME, age, GOOD_EXCITEMENT));
    }

    @Test
    void givenDolphinWhenExecutingHaveFunMethodThenExcitementIsIncremented() {
        final  Dolphin d = new Dolphin(GOOD_NAME, GOOD_AGE, GOOD_EXCITEMENT);

        final int before = d.getExcitement();
        d.haveFun();
        final int after = d.getExcitement();

        assertEquals(after, before + 1);
    }

    @ParameterizedTest
    @MethodSource("generateDolphins")
    void givenDolphinAndDolphinWhenComparingThenDolphinWithGreaterExcitementIsGreater(final Dolphin d1, final Dolphin d2) {
        if (d1.getExcitement() > d2.getExcitement()) {
            assertTrue(d1.compareTo(d2) > 0);
            assertTrue(d2.compareTo(d1) < 0);
        } else if (d1.getExcitement() < d2.getExcitement()) {
            assertTrue(d1.compareTo(d2) < 0);
            assertTrue(d2.compareTo(d1) > 0);
        } else  {
            assertEquals(0, d1.compareTo(d2));
            assertEquals(0, d2.compareTo(d1));
        }
    }


    @Test
    void givenHumanAndDolphinWhenComparingDolphinToHumanTheDolphinIsGreater() {
        final Human h = new Human(GOOD_NAME, GOOD_AGE, 100_000);
        final Dolphin d = new Dolphin(GOOD_NAME, GOOD_AGE, -100_000);
        assertTrue(d.compareTo(h) > 0, "Error: dolphins must think they are smarter than other creatures");
    }

    static private Stream<Arguments> generateDolphins() {
        return Stream.of(
                Arguments.of(new Dolphin(GOOD_NAME, GOOD_AGE, 10), new Dolphin(GOOD_NAME, GOOD_AGE, 10)),
                Arguments.of(new Dolphin(GOOD_NAME, GOOD_AGE, 100), new Dolphin(GOOD_NAME, GOOD_AGE, 124)),
                Arguments.of(new Dolphin(GOOD_NAME, GOOD_AGE, 95), new Dolphin(GOOD_NAME, GOOD_AGE, 2))
        );
    }

    static private Stream<Arguments> generateBadNames() {
        return Stream.of(
                Arguments.of(""),
                Arguments.of("     "),
                Arguments.of("\t\n\t"),
                Arguments.of("\0"),
                Arguments.of("1234ghfjgkd124")
        );
    }

    static private Stream<Arguments> generateBadAges() {
        return Stream.of(
                Arguments.of(-1),
                Arguments.of(-100),
                Arguments.of(-1000)
        );
    }

}
