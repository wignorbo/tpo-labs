package lab1.domain_model;

public class Dolphin extends Creature implements Comparable<Creature>{

    private int excitement;
    public Dolphin(final String name, final int age, final int excitement) {
        super(name, age);
        this.excitement = excitement;
    }

    public int getExcitement() {
        return this.excitement;
    }

    public void haveFun() {
        excitement++;
    }

    /***
     * Dolphins are compared according to their excitement.
     * Dolphins consider themselves smarter than any other creatures by default
     * @param o the object to be compared.
     * @return > 0 if this is smarter than o, =0 if equal, < 0 if dumber
     */
    @Override
    public int compareTo(final Creature o) {
        if (o instanceof Dolphin) {
            return this.excitement - ((Dolphin) o).excitement;
        }
        return 1;
    }
}
