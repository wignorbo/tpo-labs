package lab1;

public class JMath {

    public static final double PRECISION = 9;
    public static final double EPS = 1e-9;
    public static final double PI = 3.141_592_653_589_79;

    public static double cos(final double x) {
        final double pX = x % (2 * PI);

        double result = 0;
        double member = 1;

        for (int n = 1; Math.abs(member) >= EPS; n += 2) {
            result += member;
            member *= -pX * pX / (n * (n + 1));
        }

        return result;
    }

    public static double sec(final double x) {
        return 1 / cos(x);
    }
}
