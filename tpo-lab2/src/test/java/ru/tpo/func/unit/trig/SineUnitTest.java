package ru.tpo.func.unit.trig;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;
import ru.tpo.func.trig.Sine;

import static org.junit.jupiter.api.Assertions.*;

class SineUnitTest {
    final Sine sin = new Sine();

    @ParameterizedTest
    @CsvFileSource(resources = "/sin.csv", numLinesToSkip = 1, delimiter = ',')
    void testSine(final double arg, final double val) {
        assertEquals(val, sin.evaluate(arg), sin.eps * 10);
    }

    @ParameterizedTest
    @ValueSource(doubles = {Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NaN})
    void testSineUndefinedValues(final double arg) {
        assertEquals(sin.evaluate(arg), Double.NaN);
    }
}
