package ru.tpo.func.trig;

import ru.tpo.func.CSVWritable;

public class Cotangent extends BaseTrigFunction implements CSVWritable {

    private final Sine sin;
    private final Cosine cos;

    public Cotangent() {
        super();
        this.sin = new Sine();
        this.cos = new Cosine();
    }

    public Cotangent(final Sine sin, final Cosine cos) {
        super();
        this.sin = sin;
        this.cos = cos;
    }

    @Override
    public double evaluate(final double x, final double eps) {
        if (Math.abs(x) % Math.PI < eps) {
            return Double.NaN;
        }
        return cos.evaluate(x, eps) / sin.evaluate(x, eps);
    }
}
