package ru.tpo.func;

import ru.tpo.func.log.LogBaseN;
import ru.tpo.func.log.NaturalLog;
import ru.tpo.func.trig.Cosine;
import ru.tpo.func.trig.Cotangent;
import ru.tpo.func.trig.Secant;

public class FunctionSystem implements CSVWritable{

    private final Cosine cos;
    private final Cotangent cot;
    private final Secant sec;
    private final NaturalLog ln;
    private final LogBaseN logn;


    public FunctionSystem() {
        cos = new Cosine();
        cot = new Cotangent();
        sec = new Secant();
        ln = new NaturalLog();
        logn = new LogBaseN();
    }

    public FunctionSystem(final Cosine cos, final Cotangent cot, final Secant sec, final NaturalLog ln, final LogBaseN logn) {
        this.cos = cos;
        this.cot = cot;
        this.sec = sec;
        this.ln = ln;
        this.logn = logn;
    }

    public double evaluate(final double x, final double eps) {
        return x <= 0 ? evaluateFirst(x, eps) : evaluateSecond(x, eps);
    }

    // x <= 0 : (((((cos(x) ^ 3) ^ 2) ^ 2) ^ 2) + (cot(x) - sec(x)))
    private double evaluateFirst(final double x, final double eps) {
        return Math.pow(Math.pow(cos.evaluate(x, eps), 3), 8) + (cot.evaluate(x, eps) - sec.evaluate(x, eps));
    }

    // x > 0 : (((((ln(x) - log_10(x)) + log_5(x)) * (log_10(x) * log_5(x))) ^ 2) ^ 3)
    private double evaluateSecond(final double x, final double eps) {
        return Math.pow(Math.pow(((ln.evaluate(x, eps) - logn.evaluate(10, x, eps)) + logn.evaluate(5, x, eps)) * (logn.evaluate(10, x, eps) * logn.evaluate(5, x, eps)), 2), 3);
    }
}
