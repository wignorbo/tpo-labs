package lab1.domain_model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {


    static final int GOOD_AGE = 10;
    static final int GOOD_INTELLIGENCE = 100;
    static final String GOOD_NAME = "Name";

    @ParameterizedTest
    @MethodSource("generateBadNames")
    void givenNonAlphabeticNameWhenConstructingHumanThenIaeThrown(final String name) {
        assertThrows(IllegalArgumentException.class, () -> new Human(name, GOOD_AGE, GOOD_INTELLIGENCE));
    }

    @ParameterizedTest
    @MethodSource("generateBadAges")
    void givenBadAgeWhenConstructingHumanThenIaeThrown(final int age) {
        assertThrows(IllegalArgumentException.class, () -> new Human(GOOD_NAME, age, GOOD_INTELLIGENCE));
    }

    @Test
    void givenHumanWhenExecutingLearnMethodThenIntelIsIncremented() {
        final Human h = new Human(GOOD_NAME, GOOD_AGE, GOOD_INTELLIGENCE);

        final int before = h.getIntelligence();
        h.learnNewSkills();
        final int after = h.getIntelligence();

        assertEquals(after, before + 1);
    }

    @Test
    void givenGoodAgeAndNameWhenConstructingHumanThenSuccessfullyConstructed() {
        assertDoesNotThrow(() -> new Human(GOOD_NAME, GOOD_AGE, GOOD_INTELLIGENCE));
    }

    @ParameterizedTest
    @MethodSource("generateHumans")
    void givenHumanAndHumanWhenComparingThenHumanWithGreaterIntelIsGreater(final Human h1, final Human h2) {
        if (h1.getIntelligence() > h2.getIntelligence()) {
            assertTrue(h1.compareTo(h2) > 0);
            assertTrue(h2.compareTo(h1) < 0);
        } else if (h1.getIntelligence() < h2.getIntelligence()) {
            assertTrue(h1.compareTo(h2) < 0);
            assertTrue(h2.compareTo(h1) > 0);
        } else {
            assertEquals(0, h1.compareTo(h2));
            assertEquals(0, h2.compareTo(h1));
        }
    }

    @Test
    void givenHumanAndDolphinWhenComparingHumanToDolphinThenHumanIsGreater() {
        final Human h = new Human(GOOD_NAME, GOOD_AGE, -100_000);
        final Dolphin d = new Dolphin(GOOD_NAME, GOOD_AGE, 100_000);
        assertTrue(h.compareTo(d) > 0, "Error: humans must think they are smarter than other creatures");
    }

    static private Stream<Arguments> generateBadNames() {
        return Stream.of(
                Arguments.of(""),
                Arguments.of("     "),
                Arguments.of("\t\n\t"),
                Arguments.of("\0"),
                Arguments.of("1234ghfjgkd124")
        );
    }

    static private Stream<Arguments> generateBadAges() {
        return Stream.of(
                Arguments.of(-1),
                Arguments.of(-100),
                Arguments.of(-1000)
        );
    }

    static private Stream<Arguments> generateHumans() {
        return Stream.of(
                Arguments.of(new Human(GOOD_NAME, GOOD_AGE, 10), new Human(GOOD_NAME, GOOD_AGE, 10)),
                Arguments.of(new Human(GOOD_NAME, GOOD_AGE, 100), new Human(GOOD_NAME, GOOD_AGE, 124)),
                Arguments.of(new Human(GOOD_NAME, GOOD_AGE, 95), new Human(GOOD_NAME, GOOD_AGE, 2))
        );
    }


}
