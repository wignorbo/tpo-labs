package ru.tpo.func.trig;

import ru.tpo.func.CSVWritable;

public class Cosine extends BaseTrigFunction implements CSVWritable {

    private final Sine sin;

    public Cosine() {
        super();
        this.sin = new Sine();
    }

    public Cosine(final Sine sin) {
        super();
        this.sin = sin;
    }

    @Override
    public double evaluate(final double x, final double eps) {
        final double cos = Math.sqrt(Math.max(0, 1 - Math.pow(sin.evaluate(x, eps), 2)));
        final double mod = Math.abs(x) % (2 * Math.PI);
        if (mod <= Math.PI / 2 || mod >= 3 * Math.PI / 2) {
            return cos;
        }
        return -cos;
    }
}
