package ru.tpo.func.log;

import ru.tpo.func.CSVWritable;

public class LogBaseN extends BaseLogFunction implements CSVWritable {

    private final NaturalLog ln;

    public LogBaseN() {
        super();
        this.ln = new NaturalLog();
    }

    public LogBaseN(final NaturalLog ln) {
        super();
        this.ln = ln;
    }

    public double evaluate(final double base, final double arg) {
        return this.evaluate(base, arg, this.eps);
    }

    public double evaluate(final double base, final double arg, final double eps) {
        if (base < 0 || base == 1 || arg <= 0) {
            return Double.NaN;
        }
        return ln.evaluate(arg, eps) / ln.evaluate(base, eps);
    }
}
