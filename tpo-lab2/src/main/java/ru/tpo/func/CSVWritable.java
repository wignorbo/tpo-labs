package ru.tpo.func;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.stream.Collectors;

public interface CSVWritable {
    default void writeToCSV(final Writer out, final double... args) throws IOException {
        final String row = Arrays.stream(args)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(","));
        out.write(row);
        out.write('\n');
    }
}
