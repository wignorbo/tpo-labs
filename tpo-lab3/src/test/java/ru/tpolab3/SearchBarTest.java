package ru.tpolab3;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

class SearchBarTest {

    List<RemoteWebDriver> drivers;
    private static final String WORK_URL = "https://maps.yandex.ru";

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeEach
    public void setUp() throws MalformedURLException {
        drivers = new ArrayList<>();
        drivers.add(DriverHandler.getChromeDriver("search_bar_test"));
        drivers.add(DriverHandler.getFirefoxDriver("search_bar_test"));
    }

    @AfterEach
    public void tearDown() {
       drivers.forEach(RemoteWebDriver::quit);
    }

    @Test
    void testSidebar() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(drivers.size());
        drivers.forEach(x -> new Thread(() -> {
            try {
                testSidebarWithDriver(x);
            } catch (InterruptedException e) {
                Assertions.fail();
            } finally {
                latch.countDown();
            }
        }).start());
        latch.await();
        Assertions.assertTrue(true);
    }

    void testSidebarWithDriver(final WebDriver driver) throws InterruptedException {
        driver.get(WORK_URL);
        final SearchBar search = new SearchBar(driver);
        Thread.sleep(1000);
        search.searchQuery("контакт бар");
        final List<WebElement> elements = search.getSearchResults();
        Assertions.assertFalse(elements.isEmpty());

        search.clickOnSearchResultTitle(0);

        // title
        Assertions.assertEquals(search.getCardTitle(), "Контакт");

        // type
        Assertions.assertEquals(search.getCardType(), "Бар, паб");

        // rating
        Assertions.assertTrue(Pattern.matches("\\d,\\d", search.getCardRating()));

        // address
        Assertions.assertEquals("Невский просп., 8", search.getCardAddress());

        // review
        Assertions.assertTrue(search.getCardReviews().size() > 0);

        // photo gallery
        Assertions.assertTrue(search.getCardPhotoGallery().size() > 0);

        // route
        search.clickOnRouteButton();
        Assertions.assertTrue(search.routeFormIsVisible());
        Assertions.assertFalse(search.isRouteTypeBicycle());
        search.clickRouteTypeBicycle();
        Assertions.assertTrue(search.isRouteTypeBicycle());
        search.clickRouteTypePedestrian();
        Assertions.assertTrue(search.isRouteTypePedestrian());
        Assertions.assertFalse(search.isRouteTypeBicycle());
    }
}
