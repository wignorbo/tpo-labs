package ru.tpo.func.unit.trig;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import ru.tpo.func.trig.Secant;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SecantUnitTest {

    final Secant sec = new Secant();

    @ParameterizedTest
    @CsvFileSource(resources = "/sec.csv", numLinesToSkip = 1, delimiter = ',')
    void testSecant(final double arg, final double val) {
        assertEquals(val, sec.evaluate(arg), sec.eps * 10);
    }
}