package ru.tpo.func.integration;

import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.Mockito;
import ru.tpo.func.trig.Cosine;
import ru.tpo.func.trig.Sine;
import ru.tpo.func.util.Util;


import static org.junit.jupiter.api.Assertions.*;

class TrigIntegrationTest {
    static private Sine sin;

    @BeforeAll
    static void init() {
        sin = Mockito.mock(Sine.class);
        sin.eps = 1e-6;

        final Iterable<CSVRecord> sinCsv = Util.readCsv("src/test/resources/sin.csv");

        for (final CSVRecord record: sinCsv) {
            Mockito
                    .when(sin.evaluate(Double.parseDouble(record.get(0)), sin.eps))
                    .thenReturn(Double.valueOf(record.get(1)));
        }

    }


    @ParameterizedTest
    @CsvFileSource(resources = "/cos.csv", numLinesToSkip = 1)
    void testCos(final double arg, final double val) {
        final Cosine cos = new Cosine(sin);
        assertEquals(val, cos.evaluate(arg, cos.eps), 2 * cos.eps);
    }
}
