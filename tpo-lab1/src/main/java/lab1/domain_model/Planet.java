package lab1.domain_model;

public abstract class Planet {
    protected String name;
    protected boolean hasWater;

    protected Planet(final String name, final boolean hasWater) {
        this.name = name;
        this.hasWater = hasWater;
    }
}
