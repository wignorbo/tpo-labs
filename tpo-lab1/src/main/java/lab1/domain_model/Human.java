package lab1.domain_model;

public class Human extends Creature implements Comparable<Creature>{

    private int intelligence;
    public Human(final String name, final int age, final int intelligence) {
        super(name, age);
        this.intelligence = intelligence;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void learnNewSkills() {
        intelligence++;
    }

    /***
     * Humans are compared according to their intelligence.
     * Humans consider themselves smarter than any other creatures by default
     * @param o the object to be compared.
     * @return > 0 if this is smarter than o, =0 if equal, < 0 if dumber
     */
    @Override
    public int compareTo(final Creature o) {
        if (o instanceof Human) {
            return this.intelligence - ((Human) o).intelligence;
        }
        return 1;
    }
}
