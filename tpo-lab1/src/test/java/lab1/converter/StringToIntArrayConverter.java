package lab1.converter;

import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.util.Arrays;

public class StringToIntArrayConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(final Object s, final Class<?> target) {
        if (s instanceof String) {
            return Arrays.stream(((String) s)
                    .replace("[", "")
                    .replace("]", "")
                    .split(","))
                    .mapToInt(Integer::parseInt)
                    .toArray();
        } else {
            throw new IllegalArgumentException("Conversion unsupported.");
        }
    }
}
