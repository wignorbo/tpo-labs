package lab1;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class JMathTest {

    private static final String ASSERT_FORMAT = "Argument: %.7f, Expected: %.7f, Actual: %.7f";

    abstract static class TestTrigonometric {
        abstract void testWithBradisTable(double degrees, double radians, double expectedCos, double expectedSec);

        abstract void testPeriodicity(double radians);

        abstract static class TestEdgeCases {
            abstract void testZero();

            abstract void testPi2();

            abstract void testPi();

            abstract void test3Pi2();
        }
    }

    @Nested
    @DisplayName("Tests for lab1.JMath.cos")
    class TestCos extends TestTrigonometric {
        @ParameterizedTest(name = "lab1.JMath.cos({1}) = {2}")
        @CsvFileSource(resources = "/bradis.csv", numLinesToSkip = 1)
        @Override
        void testWithBradisTable(final double degrees, final double radians, final double expectedCos, final double expectedSec) {
            final double cos = JMath.cos(radians);
            assertAlmostEqual(expectedCos, cos, JMath.PRECISION,
                    String.format(ASSERT_FORMAT, radians, expectedCos, cos));
        }

        @ParameterizedTest(name = "lab1.JMath.cos(-{1}) = {2}")
        @CsvFileSource(resources = "/bradis.csv", numLinesToSkip = 1)
        void testNegativesWithBradisTable(final double degrees, final double radians, final double expectedCos, final double expectedSec) {
            final double cos = JMath.cos(-radians);
            assertAlmostEqual(expectedCos, cos, JMath.PRECISION,
                    String.format(ASSERT_FORMAT, -radians, expectedCos, cos));
        }

        @ParameterizedTest(name = "lab1.JMath.cos({0} + 2pi*n), 0 < n < 10")
        @ValueSource(doubles = {-2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0})
        @Override
        void testPeriodicity(final double radians) {
            final double cos = JMath.cos(radians);
            for (int n = 1; n < 10; n++) {

                final double pCos = JMath.cos(radians + 2 * n * JMath.PI);
                assertAlmostEqual(cos, pCos, JMath.PRECISION,
                        String.format(ASSERT_FORMAT, radians, cos, pCos));
            }
        }

        @Nested
        class TestEdgeCases extends TestTrigonometric.TestEdgeCases {
            @Test
            @Override
            void testZero() {
                assertEquals(1, JMath.cos(0), "cos in 0 should be 1");
            }

            @Test
            @Override
            void testPi2() {
                assertAlmostEqual(0, JMath.cos(JMath.PI / 2), JMath.PRECISION, "cos in pi/2 should be 0");
            }

            @Test
            @Override
            void testPi() {
                assertAlmostEqual(-1, JMath.cos(JMath.PI), JMath.PRECISION, "cos in pi should be -1");
            }

            @Test
            @Override
            void test3Pi2() {
                assertAlmostEqual(0, JMath.cos(3 * JMath.PI / 2), JMath.PRECISION, "cos in 3pi/2 should be 0");
            }
        }
    }

    @Nested
    @DisplayName("Tests for lab1.JMath.sec")
    class TestSec extends TestTrigonometric {

        @ParameterizedTest(name = "lab1.JMath.sec({1}) = {3}")
        @CsvFileSource(resources = "/bradis.csv", numLinesToSkip = 1)
        @Override
        void testWithBradisTable(final double degrees, final double radians, final double expectedCos, final double expectedSec) {
            final double sec = JMath.sec(radians);
            assertAlmostEqual(expectedSec, sec, JMath.PRECISION,
                    String.format(ASSERT_FORMAT, radians, expectedSec, sec));
        }

        @ParameterizedTest(name = "lab1.JMath.sec(-{1}) = {2}")
        @CsvFileSource(resources = "/bradis.csv", numLinesToSkip = 1)
        void testNegativesWithBradisTable(final double degrees, final double radians, final double expectedCos, final double expectedSec) {
            final double sec = JMath.sec(-radians);
            assertAlmostEqual(expectedSec, sec, JMath.PRECISION,
                    String.format(ASSERT_FORMAT, -radians, expectedSec, sec));
        }

        @ParameterizedTest(name = "lab1.JMath.sec({0} + 2pi*n), 0 < n < 10")
        @ValueSource(doubles = {-2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0})
        @Override
        void testPeriodicity(final double radians) {
            final double sec = JMath.sec(radians);
            for (int n = 1; n < 10; n++) {

                final double pSec = JMath.sec(radians + 2 * n * JMath.PI);
                assertAlmostEqual(sec, pSec, JMath.PRECISION,
                        String.format(ASSERT_FORMAT, radians, sec, pSec));
            }
        }

        @Nested
        class TestEdgeCases extends TestTrigonometric.TestEdgeCases {
            @Test
            @Override
            void testZero() {
                assertEquals(1, JMath.sec(0),
                        "sec in 0 should be 1");
            }

            @Test
            @Override
            void testPi2() {
                final double secPi2 = JMath.sec(JMath.PI / 2);
                assertTrue(Math.abs(secPi2) > 1 / JMath.EPS,
                        String.format("sec in pi/2 should be inf, actual: %f", secPi2));
            }

            @Test
            @Override
            void testPi() {
                assertAlmostEqual(-1, JMath.sec(JMath.PI), JMath.PRECISION,
                        "sec in pi should be -1");
            }

            @Test
            @Override
            void test3Pi2() {
                final double sec3Pi2 = JMath.sec(3 * JMath.PI / 2);
                assertTrue(Math.abs(sec3Pi2) > 1 / JMath.EPS,
                        String.format("sec in 3pi/2 should be -inf, actual: %f", sec3Pi2));
            }
        }
    }

    private static void assertAlmostEqual(final double expected, final double actual, final double precision, final String message) {
        assertTrue(Math.abs(expected - actual) < precision * 2, message);
    }
}