package util;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.Reader;
import java.util.Collections;
import java.util.List;

public class Util {
    public static List<CSVRecord> readCsv(String pathToCsv) {
        try(Reader csvFile = new FileReader(pathToCsv)) {
            return CSVFormat.DEFAULT
                    .builder()
                    .setDelimiter(',')
                    .setHeader()
                    .setSkipHeaderRecord(true)
                    .build()
                    .parse(csvFile)
                    .getRecords();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Collections.emptyList();
        }
    }
}
