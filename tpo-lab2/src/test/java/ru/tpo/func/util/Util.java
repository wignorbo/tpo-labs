package ru.tpo.func.util;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class Util {
    public static List<CSVRecord> readCsv(final String pathToCsv) {
        try (Reader csvFile = Files.newBufferedReader(Paths.get(pathToCsv))) {
            return CSVFormat.DEFAULT
                    .builder()
                    .setDelimiter(',')
                    .setHeader()
                    .setSkipHeaderRecord(true)
                    .build()
                    .parse(csvFile)
                    .getRecords();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }
}
