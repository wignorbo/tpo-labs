package ru.tpo.func.unit.trig;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import ru.tpo.func.trig.Cotangent;

import static org.junit.jupiter.api.Assertions.*;

class CotangentUnitTest {
    final Cotangent cot = new Cotangent();

    @ParameterizedTest
    @CsvFileSource(resources = "/cot.csv", numLinesToSkip = 1, delimiter = ',')
    void testCotangent(final double arg, final double val) {
        assertEquals(val, cot.evaluate(arg), cot.eps * 10);
    }
}