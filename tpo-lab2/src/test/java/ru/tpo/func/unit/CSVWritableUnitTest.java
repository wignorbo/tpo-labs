package ru.tpo.func.unit;


import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.tpo.func.CSVWritable;
import ru.tpo.func.util.Util;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class CSVWritableUnitTest {
    private static CSVWritable writable;
    private static final String TEST_FOLDER = "src/test/resources/";

    @BeforeAll
    static void createEnvironment() {
        writable = new CSVWritable() {};
    }

    @Test
    void testWriteToCsv() {
        final Path path = Paths.get(TEST_FOLDER, "file.csv");

        try (Writer writer = Files.newBufferedWriter(path)) {
            writer.write("a,b,c\n");
            writable.writeToCSV(writer, 1.0, 2.0, 3.0);
            writable.writeToCSV(writer, 4.0, 5.0, 6.0);
        } catch (IOException ignored) {
            fail();
        }

        final List<CSVRecord> list = Util.readCsv(path.toAbsolutePath().toString());
        assertEquals("1.0,2.0,3.0", list.get(0).stream().collect(Collectors.joining(",")));
        assertEquals("4.0,5.0,6.0", list.get(1).stream().collect(Collectors.joining(",")));
    }

    @AfterAll
    static void destroyEnvironment() throws IOException {
        Files.deleteIfExists(Paths.get(TEST_FOLDER, "file.csv"));
    }
}
