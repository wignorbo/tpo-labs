package ru.tpo.func.integration;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.Mockito;
import ru.tpo.func.log.LogBaseN;
import ru.tpo.func.log.NaturalLog;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class LogIntegrationTest {

    private static NaturalLog ln;
    static final double EPS = NaturalLog.EPS * 10;

    @BeforeAll
    static void init() {
        ln = Mockito.mock(NaturalLog.class);
        try (Reader lnCsv = Files.newBufferedReader(Paths.get("src/test/resources/ln.csv"))) {
            final Iterable<CSVRecord> csv = CSVFormat.DEFAULT
                    .builder()
                    .setDelimiter(',')
                    .setHeader()
                    .setSkipHeaderRecord(true)
                    .build()
                    .parse(lnCsv);

            for (final CSVRecord record : csv) {
                Mockito
                        .when(ln.evaluate(Double.parseDouble(record.get(0)), EPS))
                        .thenReturn(Double.valueOf(record.get(1)));
            }

        } catch (Exception ignored) {

        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/log10.csv", numLinesToSkip = 1)
    void testLog10(final double arg, final double val) {
        final LogBaseN base10 = new LogBaseN(ln);
        assertEquals(val, base10.evaluate(10, arg, EPS), EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/log5.csv", numLinesToSkip = 1)
    void testLog5(final double arg, final double val) {
        final LogBaseN base5 = new LogBaseN(ln);
        assertEquals(val, base5.evaluate(5, arg, EPS), EPS);
    }
}
