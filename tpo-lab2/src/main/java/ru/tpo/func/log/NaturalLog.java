package ru.tpo.func.log;

import ru.tpo.func.CSVWritable;

public class NaturalLog extends BaseLogFunction implements CSVWritable {

    public static final double EPS = 1e-8;
    public double evaluate(final double x) {
        return evaluate(x, EPS);
    }

    public double evaluate(final double x, final double eps) {
        if (x <= 0) {
            return Double.NaN;
        }

        final double multiplier = ((x - 1) * (x - 1)) / ((x + 1) * (x + 1));

        double sum = 0;
        int step = 1;
        double currentTerm = (x - 1) / (x + 1);
        while (Math.abs(currentTerm) > eps / 2) {
            sum += currentTerm;
            currentTerm = (2 * step - 1) * currentTerm * multiplier / (2 * step + 1);
            step++;
        }
        sum *= 2;
        return sum;
    }
}
