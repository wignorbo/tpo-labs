package ru.tpolab3;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DriverHandler {

    private DriverHandler() {
    }

    private static Map<String, Object> opts(final String name) {
        final HashMap<String, Object> opts = new HashMap<>();
        opts.put("name", name);
        opts.put("sessionTimeout", "15m");
        opts.put("env", List.of("TZ=UTC"));

        opts.put("labels", Map.of("manual", "true"));

        opts.put("enableVideo", true);
        opts.put("videoName", name + "-" + LocalDate.now() + ".mp4");
        opts.put("enableVNC", true);

        return opts;
    }

    public static RemoteWebDriver getChromeDriver(final String name) throws MalformedURLException {
        final RemoteWebDriver chromeDriver;
        final ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability("browserVersion", "111.0");
        chromeOptions.setCapability("selenoid:options", opts(name + "-chrome"));
        chromeOptions.addArguments("--remote-allow-origins=*");
        chromeDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chromeOptions);
        chromeDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        new WebDriverWait(chromeDriver, Duration.of(5, ChronoUnit.SECONDS)).until(
                webDriver -> "complete".equals(((JavascriptExecutor) webDriver).executeScript("return document.readyState")));


        return chromeDriver;
    }

    public static RemoteWebDriver getFirefoxDriver(final String name) throws MalformedURLException {
        final RemoteWebDriver firefoxDriver;
        final FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability("browserVersion", "111.0");
        firefoxOptions.setCapability("selenoid:options", opts(name + "-firefox"));
        firefoxDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), firefoxOptions);
        firefoxDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        new WebDriverWait(firefoxDriver, Duration.of(5, ChronoUnit.SECONDS)).until(
                webDriver -> "complete".equals(((JavascriptExecutor) webDriver).executeScript("return document.readyState")));
        return firefoxDriver;
    }
}
