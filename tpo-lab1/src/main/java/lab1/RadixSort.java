package lab1;

import java.util.Arrays;

public final class RadixSort {

    public void sort(final int... arr) {
        if (arr.length <= 1) {
            return;
        }

        final int max = Arrays.stream(arr).max().getAsInt();
        int digits = amountOfDigits(max);
        int place = 1;

        while (digits > 0) {
            digits--;
            applyCountSort(arr, place);
            place *= 10;
        }
    }

    private void applyCountSort(final int[] arr, final int place) {

        final int range = 10;
        int[] frequency = new int[range];
        int[] result = new int[arr.length];

        for (final int num : arr) {
            final int digit = (num / place) % range;
            frequency[digit]++;
        }

        for (int i = 1; i < range; i++) {
            frequency[i] += frequency[i - 1];
        }

        for (int i = arr.length - 1; i >= 0; i--) {
            final int digit = (arr[i] / place) % range;
            result[--frequency[digit]] = arr[i];
        }

        System.arraycopy(result, 0, arr, 0, arr.length);
    }

    private int amountOfDigits(final int number) {
        if (number == 0) {
            return 1;
        }

        int tmp = number;
        int result = 0;
        while (tmp > 0) {
            result++;
            tmp /= 10;
        }

        return result;
    }

}
