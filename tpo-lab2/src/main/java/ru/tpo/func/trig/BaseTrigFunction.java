package ru.tpo.func.trig;

abstract class BaseTrigFunction {
    public double eps = 1e-6;

    public double evaluate(final double x) {
        return this.evaluate(x, this.eps);
    }
    abstract public double evaluate(double x, double eps);
}
