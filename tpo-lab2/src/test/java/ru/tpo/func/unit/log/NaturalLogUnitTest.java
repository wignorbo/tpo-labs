package ru.tpo.func.unit.log;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import ru.tpo.func.log.NaturalLog;

import static org.junit.jupiter.api.Assertions.*;
class NaturalLogUnitTest {

    final NaturalLog ln = new NaturalLog();

    @ParameterizedTest
    @CsvFileSource(resources = "/ln.csv", delimiter = ',', numLinesToSkip = 1)
    void testNaturalLog(final double arg, final double res) {
        assertEquals(res, ln.evaluate(arg), NaturalLog.EPS * 10);
    }
}
