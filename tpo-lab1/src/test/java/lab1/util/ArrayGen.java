package lab1.util;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class ArrayGen {

    public static void main(final String[] args) {
        try (PrintStream stream = new PrintStream(Files.newOutputStream(Paths.get("arrays.csv")))) {
            for (int len = 1; len < 200; len++) {
                for (int n = 1; n <= 5; n++) {
                    final int[] arr = generateRandomArray(len, 0, 100_000);
                    stream.print(len + ";" + Arrays.toString(arr).replace(" ", "") + ';');
                    Arrays.sort(arr);
                    stream.println(Arrays.toString(arr).replace(" ", ""));
                }
            }
        } catch (IOException ignored) {

        }
    }

    public static int[] generateRandomArray(final int len, final int lowerBound, final int upperBound) {
        final Random r = new Random();
        return IntStream.range(0, len).map(x -> r.ints(lowerBound, upperBound).findAny().getAsInt()).toArray();
    }
}
