package lab1.domain_model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EarthTest {

    @Test
    void givenEarthWhenPopulatingWithCreaturesThenPopulationIncreases() {
        final Earth e = new Earth("Earth");

        e.addInhabitant(new Human("Name", 2, 3));
        assertEquals(1, e.getInhabitants().size());

        e.addInhabitant(new Dolphin("Name", 2, 3));
        assertEquals(2, e.getInhabitants().size());
    }

    @Test
    void givenEarthWhenTryingToAddCreaturesThroughListThenExceptionIsThrown() {
        final Earth e = new Earth("Earth");

        assertThrows(UnsupportedOperationException.class,
                () -> e.getInhabitants().add(new Dolphin("Name", 2, 3)));
    }

}
