package ru.tpo.func.integration;

import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.Mockito;
import ru.tpo.func.FunctionSystem;
import ru.tpo.func.log.LogBaseN;
import ru.tpo.func.log.NaturalLog;
import ru.tpo.func.trig.Cosine;
import ru.tpo.func.trig.Cotangent;
import ru.tpo.func.trig.Secant;
import ru.tpo.func.trig.Sine;

import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.tpo.func.util.Util.readCsv;

class FunctionSystemIntegrationTest {

    static final double EPS = 1e-4;
    static final String PATH_TO_SYSTEM_VALUES = "/system.csv";
    static private Sine sinMock;

    static private Cosine cosMock;
    static private Cotangent cotMock;
    static private Secant secMock;
    static private LogBaseN logMock;

    static private NaturalLog lnMock;


    @BeforeAll
    static void init() {
        final String basePath = "src/test/resources/";
        sinMock = Mockito.mock(Sine.class);
        cosMock = Mockito.mock(Cosine.class);
        cotMock = Mockito.mock(Cotangent.class);
        secMock = Mockito.mock(Secant.class);
        logMock = Mockito.mock(LogBaseN.class);
        lnMock = Mockito.mock(NaturalLog.class);

        final List<CSVRecord> sinValues = readCsv(Paths.get(basePath, "sin.csv").toString());
        final List<CSVRecord> cosValues = readCsv(Paths.get(basePath, "cos.csv").toString());
        final List<CSVRecord> cotValues = readCsv(Paths.get(basePath, "cot.csv").toString());
        final List<CSVRecord> secValues = readCsv(Paths.get(basePath, "sec.csv").toString());
        final List<CSVRecord> log5Values = readCsv(Paths.get(basePath, "log5.csv").toString());
        final List<CSVRecord> log10Values = readCsv(Paths.get(basePath, "log10.csv").toString());
        final List<CSVRecord> lnValues = readCsv(Paths.get(basePath, "ln.csv").toString());

        for (final CSVRecord record : sinValues) {
            Mockito.when(sinMock.evaluate(Double.parseDouble(record.get(0)), EPS)).thenReturn(Double.valueOf(record.get(1)));
        }

        for (final CSVRecord record : cosValues) {
            Mockito.when(cosMock.evaluate(Double.parseDouble(record.get(0)), EPS)).thenReturn(Double.valueOf(record.get(1)));
        }

        for (final CSVRecord record : cotValues) {
            Mockito.when(cotMock.evaluate(Double.parseDouble(record.get(0)), EPS)).thenReturn(Double.valueOf(record.get(1)));
        }

        for (final CSVRecord record : secValues) {
            Mockito.when(secMock.evaluate(Double.parseDouble(record.get(0)), EPS)).thenReturn(Double.valueOf(record.get(1)));
        }

        for (final CSVRecord record : log5Values) {
            Mockito.when(logMock.evaluate(5, Double.parseDouble(record.get(0)), EPS)).thenReturn(Double.valueOf(record.get(1)));
        }

        for (final CSVRecord record : log10Values) {
            Mockito.when(logMock.evaluate(10, Double.parseDouble(record.get(0)), EPS)).thenReturn(Double.valueOf(record.get(1)));
        }

        for (final CSVRecord record : lnValues) {
            Mockito.when(lnMock.evaluate(Double.parseDouble(record.get(0)), EPS)).thenReturn(Double.valueOf(record.get(1)));
        }
    }      

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testSystemWithMocks(final double value, final double expected) {
        final FunctionSystem function = new FunctionSystem(cosMock, cotMock, secMock, lnMock, logMock);
        assertEquals(expected, function.evaluate(value, EPS), 100 * EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testWithSec(final double value, final double expected) {
        final FunctionSystem function = new FunctionSystem(cosMock, cotMock, new Secant(cosMock), lnMock, logMock);
        assertEquals(expected, function.evaluate(value, EPS), 100 * EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testWithCot(final double value, final double expected) {
        final FunctionSystem function = new FunctionSystem(cosMock, new Cotangent(sinMock, cosMock), secMock, lnMock, logMock);
        assertEquals(expected, function.evaluate(value, EPS), 100 * EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testWithCos(final double value, final double expected) {
        final Cosine cos = new Cosine(sinMock);
        final Cotangent cot = new Cotangent(sinMock, cos);
        final Secant sec = new Secant(cos);
        final FunctionSystem function = new FunctionSystem(cos, cot, sec, lnMock, logMock);
        assertEquals(expected, function.evaluate(value, EPS), 100 * EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testWithSin(final double value, final double expected) {
        final Cosine cos = new Cosine(new Sine());
        final Secant sec = new Secant(cos);
        final Cotangent cot = new Cotangent(new Sine(), cos);
        final FunctionSystem function = new FunctionSystem(cos, cot, sec, lnMock, logMock);
        assertEquals(expected, function.evaluate(value, EPS), 100 * EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testWithLog(final double value, final double expected) {
        final FunctionSystem function = new FunctionSystem(cosMock, cotMock, secMock, lnMock, new LogBaseN(lnMock));
        assertEquals(expected, function.evaluate(value, EPS), 100 * EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testWithLn(final double value, final double expected) {
        final FunctionSystem function = new FunctionSystem(cosMock, cotMock, secMock, new NaturalLog(), new LogBaseN(new NaturalLog()));
        assertEquals(expected, function.evaluate(value, EPS), 500 * EPS);
    }

    @ParameterizedTest
    @CsvFileSource(resources = PATH_TO_SYSTEM_VALUES, numLinesToSkip = 1)
    void testWithAll(final double value, final double expected) {
        final FunctionSystem function = new FunctionSystem();
        assertEquals(expected, function.evaluate(value, EPS), 0.1);
    }
}
