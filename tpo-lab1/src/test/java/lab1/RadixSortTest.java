package lab1;

import lab1.converter.StringToIntArrayConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.NullSource;


import static org.junit.jupiter.api.Assertions.*;

class RadixSortTest {

    RadixSort radix = new RadixSort();
    @ParameterizedTest
    @NullSource
    void givenNullPtrWhenRadixSortThenNpeThrown(final int... arr) {
        assertThrows(NullPointerException.class, () -> radix.sort(arr));
    }

    @ParameterizedTest(name = "sort int[{0}]")
    @CsvFileSource(resources = "/arrays.csv", delimiter = ';')
    void givenUnsortedIntArrayWhenRadixSortThenArrayIsSorted(
            final int length,
            @ConvertWith(StringToIntArrayConverter.class) final int[] unsortedArray,
            @ConvertWith(StringToIntArrayConverter.class) final int... sortedArray)
    {
        radix.sort(unsortedArray);
        assertArrayEquals(unsortedArray, sortedArray);
    }

    @Test
    void givenEmptyArrayWhenRadixSortThenNoExceptionThrown() {
        final int[] array = new int[0];
        assertDoesNotThrow(() -> radix.sort(array));
    }
}
