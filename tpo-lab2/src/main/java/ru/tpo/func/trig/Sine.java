package ru.tpo.func.trig;

import ru.tpo.func.CSVWritable;

public class Sine extends BaseTrigFunction implements CSVWritable {
    @Override
    public double evaluate(final double x, final double eps) {
        if (x == Double.NEGATIVE_INFINITY || x == Double.POSITIVE_INFINITY || Double.isNaN(x)) {
            return Double.NaN;
        }

        // sin(x) = x - x^3/3! + x^5/5! - x^7/7! + ...

        double xTerm = x;

        double sum = xTerm;
        double prev = 0;
        long step = 1;
        while (Math.abs(sum - prev) > eps) {
            xTerm *= -Math.pow(x, 2) / (++step * ++step);
            prev = sum;
            sum += xTerm;
        }
        return sum;
    }
}
