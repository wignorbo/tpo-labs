package lab1.domain_model;

public abstract class Creature {

    private static final String VALID_NAME_REGEX = "[a-zA-Z]+";

    protected String name;
    protected int age;


    protected Creature(final String name, final int age) {
        if (!name.matches(VALID_NAME_REGEX)) {
            throw new IllegalArgumentException("Name should not be blank.");
        }
        if (age < 0) {
            throw new IllegalArgumentException("Illegal age: " + age);
        }
        this.name = name;
        this.age = age;
    }
}
