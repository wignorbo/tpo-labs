package lab1.domain_model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Earth extends Planet {
    private final List<Creature> inhabitants;

    public Earth(final String name) {
        super(name, true);
        inhabitants = new LinkedList<>();
    }

    public boolean addInhabitant(final Creature creature) {
        return inhabitants.add(creature);
    }

    public List<Creature> getInhabitants() {
        return Collections.unmodifiableList(inhabitants);
    }
}
