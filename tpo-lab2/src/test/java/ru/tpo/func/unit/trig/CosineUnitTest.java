package ru.tpo.func.unit.trig;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import ru.tpo.func.trig.Cosine;

import static org.junit.jupiter.api.Assertions.*;

class CosineUnitTest {

    final Cosine cos = new Cosine();

    @ParameterizedTest
    @CsvFileSource(resources = "/cos.csv", numLinesToSkip = 1, delimiter = ',')
    void testCosine(final double arg, final double val) {
        assertEquals(val, cos.evaluate(arg), cos.eps * 10);
    }
}