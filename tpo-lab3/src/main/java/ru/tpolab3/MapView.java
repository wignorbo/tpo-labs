package ru.tpolab3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class MapView {
    protected final WebDriver driver;
    final private By zoomInButton = By.xpath("//div[@class=\"zoom-control__zoom-in\"]");
    final private By zoomOutButton = By.xpath("//div[@class=\"zoom-control__zoom-out\"]");

    final private By optionsButton = By.xpath("//div[@class=\"map-layers-control\"]");

    final private By satelliteButton = By.xpath("//div[@class=\"segmented-control-view__item _key_satellite\"]");

    final private By hybridButton = By.xpath("//div[@class=\"segmented-control-view__item _key_hybrid\"]");



    final private By map = By.xpath("//div/ymaps");

    public MapView(final WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void zoomIn() {
        final WebElement zoomInElement = driver.findElement(zoomInButton);
        zoomInElement.click();
    }

    public void zoomOut() {
        final WebElement zoomOutElement = driver.findElement(zoomOutButton);
        zoomOutElement.click();
    }

    public void moveToOptions() {
        final WebElement optionsElement = driver.findElement(optionsButton);
        optionsElement.click();
    }

    public void clickSatellite() {
        final WebElement satelliteElement = driver.findElement(satelliteButton);
        satelliteElement.click();
    }

    public void clickHybrid() {
        final WebElement hybridElement = driver.findElement(hybridButton);
        hybridElement.click();
    }

    public void move(final int x, final int y) {
        final WebElement mapView = driver.findElement(map);
        final Actions actions = new Actions(driver);
        actions.clickAndHold(mapView);
        actions.moveByOffset(x, y);
        actions.release();
    }
}
